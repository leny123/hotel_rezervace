<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        DB::table('role_user')->truncate();

        $adminRole = Role::where('name', 'Admin')->get()->first();
        $userRole = Role::where('name', 'User')->get()->first();

        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.cz',
            'password' => Hash::make('123')
        ]);

        $user = User::create([
            'name' => 'User',
            'email' => 'user@user.cz',
            'password' => Hash::make('123')
        ]);

        $admin->roles()->attach($adminRole);
        $user->roles()->attach($userRole);
    }
}
