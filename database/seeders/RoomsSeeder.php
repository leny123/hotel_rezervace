<?php

namespace Database\Seeders;

use App\Models\Room;
use Illuminate\Database\Seeder;

class RoomsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Room::truncate();

        Room::create([
            'name' => '1 Lůžkový pokoj',
            'type' => '1 Lůžkový',
            'number' => '1',
            'active' => '1',
        ]);
        Room::create([
            'name' => '2 Lůžkový pokoj',
            'type' => '2 Lůžkový',
            'number' => '2',
            'active' => '1',
        ]);
        Room::create([
            'name' => 'Apartmán',
            'type' => 'Apartmán',
            'number' => '3',
            'active' => '1',
        ]);
    }
}
