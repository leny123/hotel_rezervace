<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'phone',
        'start',
        'end',
    ];

    public $timestamps = false;

    public function room() {
        return $this->belongsToMany(Room::class);
    }
}
