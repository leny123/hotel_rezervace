<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReservationRequest;
use App\Http\Resources\ReservationResource;
use App\Models\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //zobrazení rezervací (pouze které jsou aktivní)
        return response(ReservationResource::collection(Reservation::where('end', '>', date('d-m-Y'))->get()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReservationRequest $request)
    {
        //uložení nové rezervace
        $reservation = Reservation::create($request->validated());
        $reservation->room()->attach($request->checkedRoom);
        return response(['success' => 'Rezervace odeslána.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Reservation $reservation)
    {
        //kontrola jestli je přihlášen jako admin
        if(Auth::check()) {
            if(!$request->user()->isAdmin()) {
                abort(404); 
            }
        } else {
            abort(404);
        }
        //odebrat rezervaci
        $reservation->room()->detach();
        $reservation->delete();
        return response(['success' => 'Rezervace úspěšně odebrána.']);
    }
}
