<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoomRequest;
use App\Models\Reservation;
use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //zobrazení všech pokojů které jsou seřazeny podle čísla
        return response(Room::orderBy('number', 'ASC')->get());
    }

    public function store(RoomRequest $request)
    {
        //kontrola jestli je přihlášen jako admin
        if(Auth::check()) {
            if(!$request->user()->isAdmin()) {
                abort(404); 
            }
        } else {
            abort(404);
        }
        if(Room::where('number', $request->number)->first()) {
            return response(['error' => 'Pokoj s tímto číslem již existuje.']);
        }

        //vytvořit pokoj
        Room::create($request->validated());
        return response(['success' => 'Nový pokoj vytvořen.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Room $room)
    {
        //kontrola jestli je přihlášen jako admin
        if(Auth::check()) {
            if(!$request->user()->isAdmin()) {
                abort(404); 
            }
        } else {
            abort(404);
        }

        //odebrat pokoj
        foreach($room->reservations as $reservation) {
            $reservation->delete();
        }
        $room->reservations()->detach();
        $room->delete();
        return response(['success' => 'Pokoj byl úspěšně odebrán.']);
    }

    public function searchRooms(Request $request) {
        //pokud není zadán čas, zrušit hledání
        if($request->time[0] == null) {
            return;
        }
       
        $start_date = $request->time[0];
        $end_date = $request->time[1];
        $reservations = Reservation::all();
        $rooms = [];

        //filtrování dostupných pokojů
        if(count($reservations)) {
            foreach($reservations as $reservation) {
                //kontrola jestli zadané datum je mezi datumy rezervací
                if(($reservation->start >= $start_date) && ($reservation->start <= $end_date) || 
                   ($reservation->end >= $start_date) && ($reservation->end <= $end_date)) {
                    /* pokud je na zadané datum rezervace,
                     vzít pokoj na který je rezervace odeslána a vyřadit ho ze zobrazení */
                    array_push($rooms, $reservation->room->first()->id);
                }
            }
            if($rooms) {
                //vyřazení pokojů, které jsou již zarezervovány na zadaný čas
                $room = Room::whereNotIn('id', $rooms)->where('type', $request->room)->get();
                if($room) {
                    $roomData = $room;
                } 
            } else {
                $roomData = Room::where('type', $request->room)->get();
            }
        } else {
             $roomData = Room::where('type', $request->room)->get();
        }
        
        if(count($roomData)) {
            //dostupné pokoje, zobrazit je
            return response(['data' => $roomData, 'available' => true]);
        } else {
            //nedostupné žádné pokoje, zobrazit warning zprávu
            return response(['nodata' => 'nodata', 'available' => true]);
        }
        
    }
}
