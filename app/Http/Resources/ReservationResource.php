<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReservationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'room' => $this->room->first()->number,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'start' => $this->start,
            'end' => $this->end,
        ];
    }
}
