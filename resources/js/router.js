import HomeComponent from './components/HomeComponent'

export default {
    mode: 'history',
    routes: [
        {
            path: '/',
            component: HomeComponent
        }
    ]
}