<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::ApiResource('/rooms', RoomController::class);
Route::ApiResource('/reservations', ReservationController::class);
Route::post('/rooms/search', [RoomController::class, 'searchRooms']);
Route::post('/permissions', function(Request $request) {
    if(Auth::check()) {
        if(!$request->user()->isAdmin()) {
            return response(['data' => false]);
        } 
    } else {
        return response(['data' => false]);
    }
    return response(['data' => true]);
});